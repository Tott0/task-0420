package com.augustosalazar.androidjson;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class EditActivity extends ActionBarActivity {

    private DataEntry mDataEntry;
    private int mPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Intent i = getIntent();
        mDataEntry = (DataEntry) i.getSerializableExtra("data");
        mPosition = i.getIntExtra("position",-1);

        TextView nombre = (TextView) findViewById(R.id.tvFirstName2);
        TextView apellido = (TextView) findViewById(R.id.tvLastName2);
        TextView genero = (TextView) findViewById(R.id.tvGender2);

        nombre.setText(mDataEntry.getFistName());
        apellido.setText(mDataEntry.getLastName());
        genero.setText(mDataEntry.getGender());


        new DownloadImageTask((ImageView) findViewById(R.id.imageView2))
                .execute(mDataEntry.getPicture());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnEditClick(View view) {
        Intent data = new Intent();

        TextView nombre = (TextView) findViewById(R.id.tvFirstName2);
        TextView apellido = (TextView) findViewById(R.id.tvLastName2);
        TextView genero = (TextView) findViewById(R.id.tvGender2);

        mDataEntry.setFistName(nombre.getText().toString());
        mDataEntry.setLastName(apellido.getText().toString());
        mDataEntry.setGender(genero.getText().toString());

        data.putExtra("data",(DataEntry)mDataEntry);
        data.putExtra("position",mPosition);
        setResult(RESULT_OK, data);
        finish();

    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
