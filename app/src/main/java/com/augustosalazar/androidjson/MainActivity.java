package com.augustosalazar.androidjson;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.core.view.DataEvent;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends ActionBarActivity implements ViewAdapter.RecyclerClickListner{

    private ProgressDialog pDialog;
    //private static String url = "http://api.androidhive.info/contacts/";
    private static String url = "http://api.randomuser.me/?results=1&format=jsaon";
    JSONArray usuarios = null;

    private List<DataEntry> mData = new ArrayList<>();
    private RecyclerView mrV;
    private ViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private final String oRoot = "https://task-0420.firebaseio.com/";
    Firebase myFirebaseRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Firebase.setAndroidContext(this);
        myFirebaseRef = new Firebase(oRoot);


        mrV = (RecyclerView) findViewById(R.id.recycle);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new ViewAdapter(this,mData);
        mrV.setAdapter(mAdapter);

        mAdapter.setRecyclerClickListner(this);

        setListener();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isNetworkAvaible = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            isNetworkAvaible = true;
            Toast.makeText(this, "Network is available ", Toast.LENGTH_LONG)
                    .show();
        } else {
            Toast.makeText(this, "Network not available ", Toast.LENGTH_LONG)
                    .show();
        }
        return isNetworkAvaible;
    }


    public void requestData(View view) {

        new GetData().execute();


    }

    public void checkInternet(View view) {
        isNetworkAvailable();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:
                if(resultCode == Activity.RESULT_OK){

                    DataEntry eDataEntry = (DataEntry) data.getSerializableExtra("data");
                    int position = data.getIntExtra("position",-1);
                    myFirebaseRef.child(eDataEntry.getKey()).setValue(eDataEntry);
                    mAdapter.editItem(position, eDataEntry);

                }
                break;
        }
    }

    @Override
    public void itemClick(View view, int position) {
        Log.d("asd",mAdapter.getItem(position).getLastName());
        //callback.onFragment1EditClick((DataEntry)view.getTag());
        Intent i = new Intent(MainActivity.this,DetailActivity.class);
        i.putExtra("data",(DataEntry)mAdapter.getItem(position));
        startActivity(i);
    }

    @Override
    public void editClick(View view, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Edit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.d("asd","View index "+position);
                        //callback.onFragment1EditClick((DataEntry)view.getTag());
                        Intent i = new Intent(MainActivity.this,EditActivity.class);
                        i.putExtra("data",(DataEntry)mAdapter.getItem(position));
                        i.putExtra("position",position);
                        startActivityForResult(i, 1);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });;
        builder.show();

    }

    @Override
    public void deleteClick(final View view, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Delete?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DataEntry dataEntry = mAdapter.getItem(position);
                        myFirebaseRef.child(dataEntry.getKey()).removeValue();
                        mAdapter.removeItem(position);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });;
        builder.show();
    }


    /**
     * Async task class to get json by making HTTP call
     */
    private class GetData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    usuarios = jsonObj.getJSONArray("results");
                    Log.d("Response length: ", "> " + usuarios.length());

                    for (int i = 0; i < usuarios.length(); i++) {
                        JSONObject c = usuarios.getJSONObject(i);

                        DataEntry dataEntry = new DataEntry();

                        dataEntry.setGender(c.getString("gender"));

                        JSONObject name = c.getJSONObject("name");

                        dataEntry.setFistName(name.getString("first"));
                        dataEntry.setLastName(name.getString("last"));

                        JSONObject imageObject = c.getJSONObject("picture");

                        dataEntry.setPicture(imageObject.getString("large"));

                        mAdapter.addItem(mData.size(), dataEntry);

                        /*Map<String, String> post = new HashMap<String, String>();
                        post.put("gender",dataEntry.getGender());
                        post.put("firstName",dataEntry.getFistName());
                        post.put("last",dataEntry.getLastName());
                        post.put("email",dataEntry.getEmail());
                        post.put("picture",dataEntry.getPicture());*/
                        Firebase postFirebase = myFirebaseRef.push();
                        dataEntry.setKey(postFirebase.getKey());
                        postFirebase.setValue(dataEntry);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
        }

    }


    private void setListener(){
        myFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {

                    mAdapter.clearData();

                    for (DataSnapshot postSnapshot : snapshot.getChildren())
                    {
                        DataEntry dataEntry = postSnapshot.getValue(DataEntry.class);
                        mAdapter.addItem(mData.size(), dataEntry);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this,"Error inesperado",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCancelled(FirebaseError error) {
            }
        });
    }

}
