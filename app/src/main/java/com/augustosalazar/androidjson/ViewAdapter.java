package com.augustosalazar.androidjson; /**
 * Created by Laboratorio on 18/04/2016.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.MyViewHolder>{

    private final Context context;
    private LayoutInflater inflater;
    private List<DataEntry> data = Collections.emptyList();
    private RecyclerClickListner mRecyclerClickListner;

    public ViewAdapter(Context context,List<DataEntry> data){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        DataEntry dataEntry = data.get(position);
        holder.tv1.setText(dataEntry.getFistName());
        holder.tv2.setText(dataEntry.getLastName());

        holder.ib1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerClickListner.editClick(view, holder.getPosition());
            }
        });

        holder.ib2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerClickListner.deleteClick(view, holder.getPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void addItem(int position, DataEntry data) {
        this.data.add(position, data);
        notifyItemInserted(position);
    }

    public void editItem(int position, DataEntry data) {
        this.data.set(position, data);
        notifyItemChanged(position);
    }

    public void removeItem(int position) {
        this.data.remove(position);
        notifyItemRemoved(position);
    }

    public DataEntry getItem(int position){return this.data.get(position);

    }

    public void clearData() {
        int size = this.data.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.data.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setRecyclerClickListner(RecyclerClickListner recyclerClickListner){
        mRecyclerClickListner = recyclerClickListner;
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv1;
        private TextView tv2;
        private ImageButton ib1;
        private ImageButton ib2;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv1 = (TextView) itemView.findViewById(R.id.tvField1);
            tv2 = (TextView) itemView.findViewById(R.id.tvField2);
            ib1 = (ImageButton) itemView.findViewById(R.id.btnEdit);
            ib2 = (ImageButton) itemView.findViewById(R.id.btnDelete);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerClickListner != null) {
                mRecyclerClickListner.itemClick(v, getPosition());
            }
        }
    }

    public interface RecyclerClickListner
    {
        public void itemClick(View view, int position);

        public void editClick(View view, int position);

        public void deleteClick(View view, int position);


    }
}